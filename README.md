# symfony_docker

Quick docker conf based on [this article](https://www.twilio.com/blog/get-started-docker-symfony) to get an environement ready for a Symfony App.

## Getting started

Build environment, create your app.

## Build stack

- [ ] Build your environement with the following command:

```
docker-compose up -d --build
```

## Enter in your PHP container
```
docker-compose exec php /bin/bash
```
- [ ] You now get a terminal open on your PHP container

## Check Symfony requirements

```
symfony check:requirements
```
- [ ] You should get a Green response print in your terminal.

## Authors and acknowledgment
Based on mister [Olyuyemi Olususi]() work in this [article](https://www.twilio.com/blog/get-started-docker-symfony) (2021-05-24)

## Project status
~~~~~~~Fil rouge~~~~~~~

